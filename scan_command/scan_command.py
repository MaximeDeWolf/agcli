from docparser import new_docopt
import subprocess
import csv

def parse( doc):
    # Get options short or/and long name + default value
    usage_sections = new_docopt.parse_section('usage:', doc)[0]
    options = new_docopt.parse_defaults(doc)
    # Get relations between the commands, arguments and options: mutually exclusive, optional, ...
    pattern = new_docopt.parse_pattern(new_docopt.formal_usage(usage_sections), options)
    # Get arguments
    arguments = set(pattern.flat(new_docopt.Argument))
    #Get commands
    commands = set(pattern.flat(new_docopt.Command))
    return(set(options), arguments, commands)


def get_command_help(cmd, option='--help'):
    instruction = [cmd, option]
    proc = subprocess.run(instruction, stdout=subprocess.PIPE,
     stderr=subprocess.PIPE, universal_newlines=True)

    if proc.returncode == 0:
        return (proc.returncode, proc.stdout)
    else:
        return (proc.returncode, proc.stderr)

def parse_output(option, to_parse,to_write):
    try:
        options, arguments, commands = parse(to_parse)
        to_write['{} options'.format(option)] = len(options)
        to_write['{} arguments'.format(option)] = len(arguments)
        to_write['{} commands'.format(option)] = len(commands)
    except Exception as e:
        to_write['can parse {}'.format(option)] = False

def browse_commands(filename):
    with open(filename, 'r') as commands:
        with open('scan/scan_results.csv', 'w+', newline='') as csvfile:
            colnames = ['name', 'help handled', 'h handled', 'help options', 'h options',
             'help arguments', 'h arguments', 'help commands', 'h commands',
             'can parse help', 'can parse h']
            writer = csv.DictWriter(csvfile, fieldnames=colnames)
            writer.writeheader()
            for command in commands:
                command = command.replace('\n', '')
                help_code, help_output = get_command_help(command, option='--help')
                h_code, h_output = get_command_help(command, option='-h')
                to_write = {'name': command, 'help handled': help_code==0,
                 'h handled': h_code==0, 'help options': 0, 'h options': 0,
                 'help arguments': 0, 'h arguments': 0, 'help commands': 0,
                  'h commands': 0, 'can parse help': True, 'can parse h': True}

                if help_code==0:
                    parse_output('help', str(help_output), to_write)
                if h_code==0:
                    parse_output('h', str(h_output), to_write)

                writer.writerow(to_write)

if __name__ == '__main__':
    browse_commands("scan/commands.txt")
