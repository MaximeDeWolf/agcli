#Download base image ubuntu 18.04
FROM ubuntu:18.04

ADD ./requirements.txt /tmp/requirements.txt

# Update Software repository
# The "-y" options for "apt-get install" is mandatoty
# to automatically accept the use of more disk space
RUN apt-get update
RUN apt-get install -y \
    python3 \
    python3-pip \
    emacs \
    finger \
    talk \
    quota \
    telnet \
    ftp \
    lynx \
    man \
    lpr

RUN pip3 install --upgrade pip
RUN pip3 install -r /tmp/requirements.txt


# Set up the working dir
WORKDIR /home

#Pour build le Docker
## docker build -t sandbox .
#Pour lancer le Docker correctement:
## docker run -v $(pwd):/home/ -it sandbox /bin/bash
